# DSV - HTML

## Introducing HTML
* The core of website. The bone. The most important part. You will find it deep down every website.
* Consistent. Don't have much changes over time.
* **The differences in HTML5**
	* Focuses on building apps, drag and drop, location detection, sp drawing services.
	* More of a platform for dev lang.
* **HTML resources**
	* [w3c](https://www.w3.org/TR/html5/ "HTML5 reference")
	* [whatwg](https://html.spec.whatwg.org/multipage/ "HTML5 living standard")
	* [WebPlatform](https://webplatform.github.io/ "much nicer")
	* Mozilla Developer Network (guideline, articles, and even exercises!)
	* [HTML5 Rock](https://www.html5rocks.com/en/ "A resource for open web HTML5 developer")
	* [HTML5 Doctor](http://html5doctor.com/ "Helping you implement HTML5 today")

## Basic Page Structure:
	> A sandwich inside a sandwich.
* #### DOCTYPE declarations:
	* *(theory)* tell the version of HTML you're using
	* *(reality)* trigger 'quirks mode', ensure ur page is rendered properly
	* [DOCTYPE](https://www.w3.org/QA/2002/04/valid-dtd-list.html "Recommend list of DOCTYPE declaration")
* #### The document head
	* `meta` tag
	* `title` tag
* #### The document body
	* Visible content
* #### Content Models
	* define the type of content expected inside an element and control syntax rules.
	* **Block lvl elements**: take up their own line.
	* **Inline lvl elements**: appear w/i the flow of other content.
	* (HTML5) flow, metadata, embedded, interactive, heading, phrasing, sectioning.
	* `metadata`: setting up presentation or behavior of the rest of the content.
	* `embedded`: import other resources
	* `interactive`: user interaction
	* `heading`: header of a section (h1, h2,...)
	* `phrasing`: text
	* `flow`: thing included in normal flow of the doc.
	* `sectioning`: define the scope of heading and footer.

## Formatting page content
* #### Formating content w/ HTML:
	* `pre` tag: preformatted, mono space style, usually use for code display.
* #### Heading:
	* it's not about size, it's the structure that really matters.
* #### Formatting para:
	* vertical margins collapse
* #### Controlling line breaks: `<br>`
* #### Emphasizing text:
	* differents between `<i>` and `<em>`: only the Screen Reader knows. (they differ by logical reasons, just like `<b>` and `<strong>`)
* #### Displaying special character: search character entity reference or [here](https://dev.w3.org/html5/html-author/charref).
* #### Cotrolling whitespace: `&nbsp;` - adding whitespace, and making words break unserapately.
* #### Displaying images:
	* `<img src="" width="" height="" alt="">`
	* `alt`: altering text in case img cant/dont load

## Structuring content
* **Sectioning** element (h1.., article, aside, nav, section) & **semantic** element (header, main, footer).
* `<nav>`: create new sect specific to the nav of the site (have link to other pages or part of the page, pay attention to page's semantic reason).
* `<article>`: invidual ele that can stand alone, a separate piece of content.
* `<section>`: a generic section, with a title (stuff needs to be listed in outline, but still cant stand alone)
* `<aside>`: it related to a content of the page, but it's not THAT important to be its own section (such as sidebar, ad, pull quote,..)
* `<div>`: used for grouping content. It doesn't have a specific meaning. Very flexible.
* `<header>`: the introduction (can use many times in 1 page)
* `<footer>`: usually contain info about the section
* `<main>`: unique to each page.
* **Using WAI-ARIA roles**:
	* Helping screen reader and users. Make pages more accessible.
	* a attribute (`role=""`)
	* refer to [w3c](https://www.w3.org/TR/wai-aria/roles) and paciellogroup

## Creating Links
* **The anchor element**:
	* `href`: hypertext reference
	* `target`: control how the page opened, or where it open (w/ a frame set)
	* `rel`: the relationship between target obj and link obj
	* `title`: read by assistive devices, search engines,... description, should use.
* **Link**: must have protocol
* `target="_blank"`: open in new tab/wnd (according to browser"
* write `download` in anchor tag (`<a>`) force the browser the file
* `download="name_of_the_download_file"`
* `#id_of_the_section_in_page_you_want_to_go_to`

## Creating lists
* #### Unordered list
* #### Ordered list
		> list items would be marked w/ numbers/letters by default.
	* `start=`
	* `type="1/I/i/a/A"`
	* `reversed`
* #### Definition/Description list
	* each item consists of a term and a description
```

<dl>
	<dt></dt> <!-- (can be more than 1) -->
	<dd></dt> <!-- (can be more than 1) -->
</dl>
```

## Controlling styling
* **inline style**: only great for email and scripting
* **style element**: `<style>` in `<head>`
* **externalizing style**: `<link rel="stylesheet" href="">`

## Basic scripting
* `<script type="text/javascript">`: put at the end of the page
* **DOM**: Document Object Model
* whenever a browser encounters a script tag, it's going to stop, execute code, and resume rendering the page.
